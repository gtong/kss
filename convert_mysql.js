var mysql =require('mysql');
var $ = require('jquery');
var connection;
var http = require('http')
var fs = require('fs')
connect();
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};
function connect(){
  connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database :'yii-ks'
  });
  connection.connect();
}
function convert(){
  connection.query("SELECT * from `tbl_questions_raw` where `chs`=''", function(err, rows, fields) {
  if (err) throw err;
    for(var i=0;i<rows.length;i++){
      chs = unescape(rows[i]['raw']);
      var query = "update `tbl_questions_raw` set ? where id='"+rows[i]['id']+"'";
      // console.log(query)
      connection.query(query,{chs:chs},function(err, rows, fields){
        console.log(err)
      })
    }
  });
}

function validate_raw(){
  connection.query("select * from `tbl_questions_raw` where `status`='' limit 1000;",function(err,rows,fields){
    for(var i=0;i<rows.length;i++){
      try{
        var obj = JSON.parse(rows[i].raw)
        connection.query("update `tbl_questions_raw` set `status`='success' where `id`='"+rows[i].id+"';")
        console.log(rows[i].id+':success')
      }catch(e){
        connection.query("update `tbl_exams_items` set `status` = '' where `id`='"+rows[i].parent_id+"';")
        connection.query("update `tbl_questions_raw` set `status`='fail' where `id`='"+rows[i].id+"';")
        console.log(rows[i].id+':fail')
      }  
    }
    validate_raw()
  })
}
function convert_raw(){
  connection.query("select * from `tbl_questions_raw` where `status`='success' order by id desc limit 1;",function(err,rows,fields){
    for(var i=0;i<rows.length;i++){
      var ok = false;
      console.log(rows[i].id)  
      try{
        var obj = JSON.parse(rows[i].raw)
        for(var j=0;j<obj.Rules.length;j++){
          for(var k=0;k<obj.Rules[j].examlist.length;k++){
            ok = true;
            console.log(obj.Rules[j].examlist[k].ExamID)
            obj.Rules[j].examlist[k].parent_id = rows[i].id;
            save_questions_item_raw(obj.Rules[j].examlist[k])
          }
          obj.Rules[j].parent_id = rows[i].id;
          save_questions_title_raw(obj.Rules[j]);
        }
        if(ok==true){
          connection.query("update `tbl_questions_raw` set `status`='convert' where `id`='"+rows[i].id+"'",function(err){
            console.log(err);
          });  
        }else{
          connection.query("update `tbl_questions_raw` set `status`='convert_fail' where `id`='"+rows[i].id+"'",function(err){
            console.log(err);
          });  
        }
        convert_raw();
      }catch(e){
        connection.query("update `tbl_questions_raw` set `status`='convert_fail' where `id`='"+rows[i].id+"'",function(err){
          console.log(err);
        });
        convert_raw();
      }  
    }
  })
}
function save_questions_title_raw(obj){
  delete obj.examlist;
  for(var i in obj){
    obj[i]=unescape(obj[i])
  }
  // console.log(obj)
  connection.query("insert into `tbl_questions_title_raw` set ?",obj,function(err){
    console.log(err);
  });
}
function save_questions_item_raw(obj){
  for(var i in obj){
    obj[i]=unescape(obj[i])
  }
  // console.log(obj)
  connection.query("insert into `tbl_questions_items_raw` set ?",obj,function(err){
    console.log(err);
  });
}
function update_content_type_exam_type_one(){
  connection.query("select * from `tbl_questions_items_raw` where `content_type` ='' and `Exam_Type`= '1' limit 10000",function(err,rows,fields){
    for(var i=0;i<rows.length;i++){
      update_content_type(rows[i]);
    }
    update_content_type_exam_type_one();
  })
}
function update_content_type(row){
  try{
    var el = $("<div>"+row.Content+"</div>");
    var content_type ;
    if(el.find("img").length){
      content_type = 'img';
    }else{
      content_type = 'text';
    }
    console.log(row.id+":"+content_type)
    var query = "update `tbl_questions_items_raw` set `content_type`='"+content_type+"' where `id`='"+row.id+"'";
    connection.query(query,function(err){

    })
  }catch(e){
  }
}
function convert_exam_type_one_text(){
  connection.query("select * from `tbl_questions_items_raw` where `content_type`='text' and `json`='' and `Exam_Type` = '1' order by 'id' limit 10000",function(err,rows,fields){
    for(var i=0;i<rows.length;i++){

      var s = rows[i].Content;
      var arr = s.split("{Page}");
      if(arr.length ==2){
        var obj = {
          title:arr[0].replace(/\n/g, ""),
          choices:[]
        }
        arr[1] = arr[1].replace(/&nbsp;/,"").replace(/<br>/g,'<BR>').replace(/<br \/>/g,'<BR>').replace(/<br\/>/g,'<BR>').replace(/<BR\/>/g,'<BR>').replace(/<BR \/>/g,'<BR>').split("<BR>");
        for(var j=0;j<arr[1].length;j++){
          if(arr[1][j].replace(/\n/g, "")!=""){
            obj.choices.push(arr[1][j].replace(/\n/g, ""));  
          }
        }
        if(obj.choices.length>0){
          var s = JSON.stringify(obj)
          console.log(rows[i].id+":adding");
            connection.query("update `tbl_questions_items_raw` set ? where `id` = '"+rows[i].id+"'",{json:JSON.stringify(obj)},function(err,rows){
              if(err==null){
                console.log(":success");
              }else{
                // console.log(err)
                console.log(":fail");
              }
            })  
        }else{
          console.log(rows[i].id+":no text_fail_no_choices");
          connection.query("update `tbl_questions_items_raw` set `content_type`='text_fail_no_choices' where `id`='"+rows[i].id+"'")
        }
      }else{
        console.log(rows[i].id+":text_fail_pages");
        connection.query("update `tbl_questions_items_raw` set `content_type`='text_fail_pages' where `id`='"+rows[i].id+"'")
      }
    }
    convert_exam_type_one_text()
  })
}
function update_content_type_text_fail(){
  connection.query("select * from `tbl_questions_items_raw` where `content_type`='text_fail_pages' and `Exam_Type` = '1' order by 'id' limit 30000",function(err,rows,fields){
    for(var i=0;i<rows.length;i++){
      var s = rows[i].Content;
      var arr = s.split("{Page}");
      var type;
      if(arr.length <=2){
        type = 'text_fail_pages_zero';
        connection.query("update `tbl_questions_items_raw` set `content_type`='text_fail_pages_zero' where `id`='"+rows[i].id+"'")
      }else{
        type = 'text_fail_pages_more';
        connection.query("update `tbl_questions_items_raw` set `content_type`='text_fail_pages_more_"+arr.length+"' where `id`='"+rows[i].id+"'")
      }
      console.log(rows[i].id+":"+type)
    }
    update_content_type_text_fail()
  })
}
function set_image_src(){
  connection.query("select * from `tbl_questions_items_raw` where `content_type`='img' and `src`='' order by 'id' limit 10000",function(err,rows,fields){
    for(var i=0;i<rows.length;i++){
      var el = $("<div>"+rows[i].Content+"</div>");
      var src = [];
      el.find("img").each(function(){
        src.push($(this).attr("src"));
      })
      console.log("set src for :"+rows[i].id)
      connection.query("update `tbl_questions_items_raw` set ? where `id`='"+rows[i].id+"'",{src:JSON.stringify(src)})
    }
    set_image_src()
  })
}
function fetch_images(){
  connection.query("select * from `tbl_questions_items_raw` where `src`!='' and `src_status`='' order by 'id' limit 1",function(err,rows,fields){
    for(var i=0;i<rows.length;i++){
      var images = JSON.parse(rows[i].src);
      for(var j=0;j<images.length;j++){
        var dest = images[j].replace(/\//g,'_')
        var options = {
          host: 'http://ks.233.com',
          port: 80,
          path: images[j]
        }
        console.log(images[j])
        console.log("before")
        var request = http.get(options, function(res){
          console.log(res)
          res.setEncoding('binary')

          var imagedata = ''
          res.on('data', function (chunk) {
            console.log("data")
            imagedata += chunk}
          )

          res.on('end', function(){
            console.log("end")
            fs.writeFile(dest, imagedata, 'binary', function (err) {
              if(err){throw err}
              console.log('It\'s saved!');
            })
          })
        })
      }
    }
    // fetch_images()
  })
}
// convert_raw();
set_image_src();
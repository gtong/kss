require 'rubygems'
require 'mechanize'
require 'uri'
require 'mongo'
class Ks
	def initialize
		@db = Mongo::Connection.new("localhost",27017).db("ks")
		@level1 = @db.collection("level1")
		@level2 = @db.collection("level2")
		@level3 = @db.collection("level3")
		@level4_history = @db.collection("level4_history")
		@level4 = @db.collection("level4")
		@level5 = @db.collection("level5")
		@error = @db.collection("error")
		@root = "http://ks.233.com/"
		@login = "http://wx.233.com/login/"
		@all = "http://ks.233.com/all/"
		@redirect = "http://wx.233.com/search/member/visitors.asp?redirectURL="
		@agent = Mechanize.new
		# add_cookie
		@user = {:name=>"gabrieltong",:pw=>"ty19830222"}
	end
	def save_level1(obj)
		_save(@level1,obj)
	end
	def save_level2(obj)
		_save(@level2,obj)
	end
	def save_level3(obj)
		_save(@level3,obj)
	end
	def save_level4(obj)
		_save(@level4,obj)
	end
	def save_level5(obj)
		item = @level5.find(obj)
		if item.to_a.size == 0
			@level5.save(obj) 
			p "Save level5 successful #{obj['href']}"
		else
			p "Save level5 failed : existed #{obj['href']}"
		end
							
	end
	def _save(collection,obj)
		if obj[:href]=="#" || obj[:name]=="" 
			return
		end
		item = collection.find({:name=>obj[:href]})
		collection.save(obj) if item.to_a.size == 0		
	end
	def add_cookie
		cookies = [
			{
				:key=>"ASPSESSIONIDQCSTADSS",
				:value=>"CNMOPCICPGOEEGLMIKIGJGAL",
				:domain=>"ks.233.com"
			},
			{
				:key=>"CNZZDATA840328",
				:value=>"cnzz_eid=68180478-1347288987-&ntime=1347288987&cnzz_a=1&retime=1347289445563&sin=&ltime=1347289445563&rtime=0",
				:domain=>"ks.233.com"
			},
			{
				:key=>"Hm_lpvt_8c45d7fed7660d80a79442b673daef31",
				:value=>"1347289446278",
				:domain=>"ks.233.com"
			},
			{
				:key=>"Hm_lvt_8c45d7fed7660d80a79442b673daef31",
				:value=>"1347289430152,1347289446278",
				:domain=>"ks.233.com"
			},
			{
				:key=>"233Data",
				:value=>"password=54c0a14fc7e1a4a219ea0e9ca4bc5288&username=gabrieltong",
				:domain=>".233.com"
			},
			{
				:key=>"wxUserName",
				:value=>"gabrieltong",
				:domain=>".233.com"
			},
			{
				:key=>"CNZZDATA1527823",
				:value=>"cnzz_eid=82798369-1347280392-&ntime=1347280392&cnzz_a=0&retime=1347289111228&sin=&ltime=1347289111228&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"CNZZDATA3583776",
				:value=>"http%253A%252F%252Fwx.233.com%252Flogin%252F&ntime=1347289130&cnzz_a=1&retime=1347289435593&sin=none&ltime=1347289435593&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"CNZZDATA4109769",
				:value=>"cnzz_eid=53723152-1347289130-http%253A%252F%252Fwx.233.com%252Flogin%252F&ntime=1347289130&cnzz_a=1&retime=1347289435624&sin=none&ltime=1347289435624&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"Dozedu%5Fcom",
				:value=>"L%5FBgNum=0&Money=0&Eb=0&CourseNum=0&MemberLevel=0&OrderNum=0&LastLoginIP=210%2E177%2E98%2E223&LastLoginTime=2012%2F9%2F10+17%3A31%3A02&PassWord=54c0a14fc7e1a4a219ea0e9ca4bc5288&Guide=1&IsVIP=0&Username=gabrieltong",
				:domain=>"wx.233.com"
			}
		]
		cookies.each do |c|
			cookie = Mechanize::Cookie.new(c[:key], c[:value])
			cookie.path = "/"
			cookie.domain = c[:domain]
			cookie.for_domain = true
			@agent.cookie_jar.add!(cookie)
		end
	end
	def curl_level1
		page = @agent.get(@all)
		types = page.search(".shiti_list .title0 a")
		types.each do |a|
			obj = {"name"=>a.content,"href"=>a.attr("href")}
			save_level1(obj)
		end

	end
	def curl_level2
		@level1.find.each do |l|
			page = @agent.get(@root+l["href"])
			page.search(".most-s-c a").each_with_index do |link,i|
				if i==0
					next
				end
				obj = {"name"=>link.content,"href"=>link.attr("href"),"level1"=>l}
				@level2.save(obj)
			end
		end
	end
	def curl_level3
		@level2.find.each do |l|
			p l["href"]
			next
			page = @agent.get(l["href"])
			page.search(".most-s-c a").each_with_index do |link,i|
				if i==0
					next
				end
				obj = {"name"=>link.content,"href"=>link.attr("href"),"level2"=>l}
				@level3.save(obj)
			end
		end
	end
	def curl_level4
		@level3.find({:finished=>false}).each do |l|
			p "Curling Level4 #{l['name']}:#{l['href']}"
			curl_level4_item(l['href'],l)
			@level3.update({"_id"=>l["_id"]},{"$set" =>{:finished=>true}})
		end
	end
	def curl_level4_item(url,level)
		
		if @level4_history.find({"url"=>url,"finished"=>true}).to_a.size!=0
			return
		end
		begin
			page = @agent.get(url)
		rescue
			p "Curl #{url} failed"
			@error.save({:href=>url,:type=>"curl_level4_item"})
			return
		end
		p "Curl #{url} started"
		page.search(".shiti_list .hangs").each do |item|
			begin 
				href = item.css("a")[1].attr("href")
				name = item.css('a')[1].content
				count = item.css(".tite_2")[0].content
				publish = item.css(".w75")[-1].content
				obj = {
					:href =>href,
					:name =>name,
					:count=>count,
					:publish=>publish,
					:level3=>level
				}
				save_level4(obj)
				p "Curl #{href} successful"
			rescue
				p "Curl #{href} failed"
				@error.save({:href=>url,:type=>"curl_level4_item"})
			end
		end
		@level4_history.save({:url=>url,:finished=>true})
		manu = page.search(".manu")
		begin
			current = manu.search(".current")[0].content.to_i
			manu.search("a").each do |a|
				num = a.content.to_i
				if num>current
					curl_level4_item(a.attr('href'),level)
				end
			end
		rescue
			@error.save({:href=>url,:type=>"curl_level4_pagination"})
		end
	end
	def curl_level5
		# part_size = 10
		# size = @level4.find({"finished"=>false}).count()
		# part = (size/part_size).to_i
		# part_size.times do |i|
			@level4.find({"finished"=>"locked"}).each do |item|			
				begin			
					p "."*100
					p "Curl Level5 #{item['href']} start "
					@level4.update({"_id"=>item["_id"]},{"$set" =>{:finished=>"locked"}})
					page = @agent.get(@redirect+item["href"])
					i = 0
					page.search("#xuanz input").each do |input|
						obj = {
							"title"=>input.attr("title"),
							"value"=>input.attr("value"),
							"id"=>input.attr("id"),
							"val"=>input.attr("val"),
							"allnum"=>input.attr("allnum"),
							"levle4"=>item,
							"finished"=>false
						}
						i = i+1
						p obj["title"]
						save_level5(obj)
					end
					if i!=0
						p "Curl Level5 #{item['href']} successful "
						@level4.update({"_id"=>item["_id"]},{"$set" =>{:finished=>true}})
					else
						p "Curl Level5 #{item['href']} failed : no tab found "
					end
				rescue
					p "Curl Level5 #{item['href']} failed "
					@error.save({:href=>item["href"],:type=>"curl_level5_tabs"})
				end
			end
	# 	end
	end
	def login
		page = @agent.get(@login)
		form =  page.forms.first
		form.UserName = @user[:name]
		form.passwd = @user[:pw]
		page = @agent.submit(form,form.buttons.first)
	end
	def test
		page = @agent.get("http://www.google.com")
		# page = page.link_with(:text=>'Gmail').click
		form = page.form('f')
		form.q = "gabriel"
		page = @agent.submit(form,form.buttons.first)
	end
end
ks = Ks.new
ks.curl_level5
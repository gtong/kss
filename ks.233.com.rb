require 'rubygems'
require 'mechanize'
require 'uri'
require 'mongo'
require 'mysql2'
require "open-uri"
require "json"
class Ks233
	def initialize
		@root = "http://ks.233.com/"
		@login = "http://wx.233.com/login/"
		@guest = "http://wx.233.com/search/UserCenter/?guest=1"
		@all = "http://ks.233.com/all/"
		@redirect = "http://wx.233.com/search/member/visitors.asp?redirectURL="
		setup_mysql()
		setup_agent()
	end
	def setup_agent
		@agent = Mechanize.new
		add_cookie
	end
	def setup_mysql
		@conn = Mysql2::Client.new(:host => "127.0.01", :username => "root",:password=>'',:database=>'yii-ks')
		# @conn2 = Mysql2::Client.new(:host => "127.0.01", :username => "root",:password=>'',:database=>'yii-ks-old')
	end
	def save_level1(obj)
		p "Adding level1:#{obj['title']}"
		result = @conn.query("select * from `tbl_tree` where `title`='#{obj['title']}';")
		if result.size == 0
			@conn.query("insert into `tbl_tree` (title,href,parent_id,status) values ('#{obj['title']}','#{obj['href']}','','')")
			p "Adding level1:#{obj['title']}:success"
		else
			p "Adding level1:#{obj['title']}:fail:exist"
		end
	end
	def save_level2(obj)
		p "Adding level2:#{obj['title']}"
		result = @conn.query("select * from `tbl_tree` where  `href`='#{obj['href']}';")
		if result.size == 0
			@conn.query("insert into `tbl_tree` (title,href,parent_id,status) values ('#{obj['title']}','#{obj['href']}','#{obj['parent_id']}','');")
			p "Adding level2:#{obj['title']}:success"
		else
			p "Adding level2:#{obj['title']}:fail:exist"
		end
	end
	def save_level3(obj)
		p "Adding level3:#{obj['title']}"
		result = @conn.query("select * from `tbl_tree` where  `href`='#{obj['href']}' ;")

		if result.size == 0
			@conn.query("insert into `tbl_tree` (title,href,parent_id,status) values ('#{obj['title']}','#{obj['href']}','#{obj['parent_id']}','');")
			p "Adding level3:#{obj['title']}:success"
		else
			p "Adding level3:#{obj['title']}:fail:exist"
		end
	end
	def save_level4(obj)
		p "Adding level4:#{obj['title']}"
		result = @conn.query("select * from `tbl_exams` where  `href`='#{obj['href']}' ;")
		if result.size == 0
			query = "insert into `tbl_exams` (title,href,publish,parent_id,status) values ('#{obj[:title]}','#{obj[:href]}','#{obj[:publish]}','#{obj[:parent_id]}','');"
			@conn.query(query)
			p "Adding level4:#{obj['title']}:success"
		else
			p "Adding level4:#{obj['title']}:fail:exist"
		end
	end
	def save_level5(obj)
		item = obj
		p "Adding level5:#{obj['title']}"
		begin
			query = "insert into `tbl_exams_items` (title,parent_id,ajax_id,val,value,allnum,status) values ('#{item['title']}','#{item['parent_id']}','#{item['ajax_id']}','#{item['val']}','#{item['value']}','#{item['allnum']}','');"
			@conn.query(query)
			p "Adding level5:#{obj['title']}:success"
		rescue
			p "Adding level5:#{obj['title']}:fail:exist"
		end			
	end
	def save_level6(obj)
		begin
			query = "insert into `tbl_questions_raw` (parent_id,raw,status) values ('#{obj['parent_id']}','#{obj['raw']}','');"
			@conn.query(query)
			p "Adding level6:#{obj['parent_id']}:success"
		rescue
			p "Adding level6:#{obj['parent_id']}:fail:exist"
		end		
	end
	def save_top_simulate_item(obj)
		item = obj
		begin
			# query = "insert into `tbl_top_simulates` ('parent_id','href','title','publish','used') values ('#{item['parent_id']}','#{item['href']}','#{item['title']}','#{item['publish'],'#{item['used']}');"
			query = "insert into `tbl_top_simulates` (parent_id,href,title,publish,used) values ('#{item['parent_id']}','#{item['href']}','#{item['title']}','#{item['publish']}','#{item['used']}');"
			@conn.query(query)
			p "Adding top_simulate:#{obj['href']}:success"
		rescue
			p "Adding top_simulate:#{obj['href']}:fail:exist"
		end		
	end
	def _save(collection,obj)
		if obj[:href]=="#" || obj[:name]=="" 
			return
		end
		item = collection.find({:name=>obj[:href]})
		collection.save(obj) if item.to_a.size == 0		
	end
	def add_cookie
		cookies = [
			{
				:key=>"CNZZDATA1527823",
				:value=>"cnzz_eid=32793556-1347412656-http%253A%252F%252Fks.233.com%252F&ntime=1347412656&cnzz_a=1&retime=1347416444079&sin=http%253A%252F%252Fks.233.com%252F&ltime=1347416444079&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"CNZZDATA3583776",
				:value=>"cnzz_eid=91239576-1347412787-http%253A%252F%252Fwx.233.com%252Fsearch%252FUserCenter%252F&ntime=1347412787&cnzz_a=1&retime=1347416458061&sin=none&ltime=1347416458061&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"CNZZDATA4109769",
				:value=>"cnzz_eid=32069504-1347416404-http%253A%252F%252Fwx.233.com%252Fsearch%252FUserCenter%252F&ntime=1347416404&cnzz_a=1&retime=1347416458034&sin=none&ltime=1347416458034&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"Dozedu%5Fcom",
				:value=>"L%5FBgNum=0&Guide=1&IsVIP=0&Username=javascriptdaily&PassWord=bdfed55722947570347ed5261d2ece6a&LastLoginTime=2012%2F9%2F12+10%3A19%3A59&LastLoginIP=210%2E177%2E98%2E223&OrderNum=0&MemberLevel=0&CourseNum=0&Eb=0&Money=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"233AspNet",
				:value=>"UN=javascriptdaily&mcount=8&nowmcount=8&t=2012/9/12 10:20:05",
				:domain=>"wx.233.com"
			},
			{
				:key=>"233doNet",
				:value=>"TaskNum=17",
				:domain=>"wx.233.com"
			},
			{
				:key=>"ASP.NET_SessionId",
				:value=>"xh54pzq0qb5jqtybmavysa55",
				:domain=>"wx.233.com"
			},
			{
				:key=>"ASPSESSIONIDCSBDSSRD",
				:value=>"LDDDIENDEANGGIIJHKGECAFF",
				:domain=>"wx.233.com"
			},
			{
				:key=>"LogErrNum",
				:value=>"0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"jsusername",
				:value=>"javascriptdaily",
				:domain=>"wx.233.com"
			},
			{
				:key=>"233%5Fcom",
				:value=>"CourseNum=0&OrderNum=0&password=bdfed55722947570347ed5261d2ece6a&username=javascriptdaily",
				:domain=>".233.com"
			},
			{
				:key=>"233Data",
				:value=>"password=bdfed55722947570347ed5261d2ece6a&username=javascriptdaily",
				:domain=>".233.com"
			},
			{
				:key=>"wxUserName",
				:value=>"javascriptdaily",
				:domain=>".233.com"
			}
		]
		cookies.each do |c|
			cookie = Mechanize::Cookie.new(c[:key], c[:value])
			cookie.path = "/"
			cookie.domain = c[:domain]
			cookie.for_domain = true
			@agent.cookie_jar.add!(cookie)
		end
	end
	def curl_level1
		page = @agent.get(@all)
		types = page.search(".shiti_list .title0 a")
		types.each do |a|
			obj = {"title"=>a.content,"href"=>a.attr("href")}
			save_level1(obj)
		end
	end
	def curl_level2
		result = @conn.query("select * from `tbl_tree` where `parent_id`='';")
		result.each do |l|
			page = @agent.get(@root+l["href"])
			page.search(".most-s-c a").each_with_index do |link,i|
				if i==0
					next
				end
				obj = {"title"=>link.content,"href"=>link.attr("href"),"parent_id"=>l['id']}
				save_level2(obj)
			end
		end
	end
	def curl_level3
		roots = @conn.query("select * from `tbl_tree` where `parent_id`='';")
		roots.each do |root|
			seconds = @conn.query("select * from `tbl_tree` where `parent_id`='#{root['id']}';")
			seconds.each do |second|
				page = @agent.get(second["href"])
				page.search(".most-s-c a").each_with_index do |link,i|
					if i==0
						next
					end
					obj = {"title"=>link.content,"href"=>link.attr("href"),"parent_id"=>second['id']}
					save_level3(obj)
				end
			end
		end
	end
	def curl_level4
		roots = @conn.query("select * from `tbl_tree` where `parent_id`='';")
		roots.each do |root|
			seconds = @conn.query("select * from `tbl_tree` where `parent_id`='#{root['id']}';")
			seconds.each do |second|
				thirds = @conn.query("select * from `tbl_tree` where `parent_id`='#{second['id']}' and `status`!='true';")
				thirds.each do |third|
					curl_level4_item(third['href'],third)
					@conn.query("update `tbl_tree` set `status`='true' where `id`='#{third['id']}' ");
				end
			end
		end
	end
	def curl_level4_clear_history
		roots = @conn.query("select * from `tbl_tree` where `parent_id`='';")
		roots.each do |root|
			seconds = @conn.query("select * from `tbl_tree` where `parent_id`='#{root['id']}';")
			seconds.each do |second|
				thirds = @conn.query("select * from `tbl_tree` where `parent_id`='#{second['id']}';")
				thirds.each do |third|
					@conn.query("update `tbl_tree` set `status`='false' where `id`='#{third['id']}' ");
				end
			end
		end
	end
	def curl_level4_item(url,level)
		query = "select * from `tbl_page_histories` where `href`='#{url}';"
		result = @conn.query(query)
		if result.size!=0
			return
		end
		begin
			p "Curl #{url} started"
			page = @agent.get(url)
		rescue
			p "Curl #{url} failed"
			return
		end
		page.search(".shiti_list .hangs").each do |item|
			begin 
				href = item.css("a")[1].attr("href")
				title = item.css('a')[1].content
				count = item.css(".tite_2")[0].content
				publish = item.css(".w75")[-1].content
				obj = {
					:href =>href,
					:title =>title,
					:publish=>publish,
					:parent_id=>level['id']
				}
				save_level4(obj)
			rescue
			end
		end
		query = "insert into `tbl_page_histories` (href,type,status) values ('#{url}','level4_item','true');"
		@conn.query(query)
		manu = page.search(".manu")
		begin
			current = manu.search(".current")[0].content.to_i
			manu.search("a").each do |a|
				num = a.content.to_i
				if num>current
					curl_level4_item(a.attr('href'),level)
				end
			end
		rescue

		end
	end
	def curl_level5
		# query = "select * from `tbl_exams` where `status`!='true' order by `id`;"
		query = "select * from `tbl_exams` where `status`!='true' order by `id` desc;"
		result = @conn.query(query)
		result.each do |item|	
			if item["status"] == 'true'
				p "Already finished"
				next
			end
			begin			
				p "Curl Level5 #{item['href']} start "
				page = @agent.get(@redirect+item["href"])
				i = 0
				page.search("#xuanz input").each do |input|
					obj = {
						"title"=>input.attr("title"),
						"value"=>input.attr("value"),
						"ajax_id"=>input.attr("id"),
						"val"=>input.attr("val"),
						"allnum"=>input.attr("allnum"),
						'parent_id'=>item['id']
					}
					i = i+1
					save_level5(obj)
				end
				if i!=0
					p "Curl Level5 #{item['href']} successful "
					@conn.query("update `tbl_exams` set `status` = 'true' where `id`='#{item['id']}';")
				else
					p "Curl Level5 #{item['href']} failed : no tab found "
				end
			rescue
				p "Curl Level5 #{item['href']} failed : can't open page"
			end	
		end
	end
	def curl_level6
		begin
			curl_level6_item
		rescue
			curl_level6
		end
	end
	def curl_level6_item
		result = @conn.query("select * from `tbl_exams_items` where `status` != 'true';")
		result.each do |item|
			curl_level6_item_by_item(item)
		end
		curl_level6
	end
	def curl_level6_item_by_item(item)
		begin
			p item['id']
			parents = @conn.query("select * from `tbl_exams` where `id`='#{item['parent_id']}';")
			parents.each do |parent|
				pageID = parent["href"].gsub("http://wx.233.com/search/UserCenter/examCenter/answer.asp?PaperID=","").gsub("&guest=1","").to_i
				p pageID
				href = "http://wx.233.com/search/UserCenter/task/exam/ExamServer.ashx?Act=ViewAnswer&RulesID=#{item['ajax_id']}&PaperID=#{pageID}&_=1347410627224"
				p href
				page = @agent.get(href)
				raw = page.search("body")[0].content
				obj ={'raw'=>raw,'parent_id'=>item['id'],'status'=>''}
				if obj['raw'].to_s == "{\"S\":\"0\"}"
					next
				end
				save_level6(obj)
				@conn.query("update `tbl_exams_items` set `status`='true' where `id`='#{item['id']}';")
			end
		rescue
		end
	end
	def curl_top_simulate
		url = 'http://ks.233.com/top/3/'
		curl_top_simulate_item(url)
	end
	def curl_top_simulate_item(url)
		begin
			p "Curl #{url} started"
			page = @agent.get(url)
		rescue
			p "Curl #{url} failed"
			return
		end
		page.search('.shiti_list .cont .hangs').each do |item|
			begin 
				href = item.css("a.cBlue")[0].attr("href")
				title = item.css("a.cBlue")[0].attr("title")
				used = item.css('.w75 span')[0].content
				publish = item.css(".w75")[1].content
				parents = @conn.query("select * from `tbl_exams` where `href`='#{href}';")
				if parents.size == 0
					parent_id = 0
				else
					parent_id = parents.first['id']
				end
				obj = {
					"parent_id"=>parent_id,
					"href" =>href,
					"title" =>title,
					"publish"=>publish,
					"used"=>used
				}
				save_top_simulate_item(obj)
			rescue

			end
		end
		# page.search(".shiti_list .hangs").each do |item|
		# 	begin 
		# 		href = item.css("a")[1].attr("href")
		# 		title = item.css('a')[1].content
		# 		count = item.css(".tite_2")[0].content
		# 		publish = item.css(".w75")[-1].content
		# 		obj = {
		# 			:href =>href,
		# 			:title =>title,
		# 			:publish=>publish,
		# 			:parent_id=>level['id']
		# 		}
		# 		save_level4(obj)
		# 	rescue
		# 	end
		# end
		# query = "insert into `tbl_page_histories` (href,type,status) values ('#{url}','level4_item','true');"
		# @conn.query(query)
		# manu = page.search(".manu")
		# begin
		# 	current = manu.search(".current")[0].content.to_i
		# 	manu.search("a").each do |a|
		# 		num = a.content.to_i
		# 		if num>current
		# 			curl_level4_item(a.attr('href'),level)
		# 		end
		# 	end
		# rescue

		# end
	end
	def convert2
		@conn.query("select * from `tbl_tree` where `parent_id`='#{obj['title']}';")
		@level2.find.each do |item|
			begin
				p "convert level2 #{item['_id']}"
				item["level1_id"] = item["level1"]["_id"]
				item.delete("level1")
				@level2.update({"_id"=>item["_id"]},item)
			rescue
			end
		end
	end
	def convert3
		@level3.find.each do |item|
			begin
				p "convert level3 #{item['_id']}"
				item["level2_id"] = item["level2"]["_id"]
				@level3.update({"_id"=>item["_id"]},item)
			rescue
			end
			begin
				item.delete("level2")
				@level3.update({"_id"=>item["_id"]},item)
			rescue
			end
			
		end
	end	
	def convert4
		@level4.find.each do |item|
			begin
				p "convert level4 #{item['_id']}"
				item["level3_id"] = item["level3"]["_id"]
				@level4.update({"_id"=>item["_id"]},item)
			rescue
			end
			begin
				item.delete("level3")
				item.delete("finished")
				@level4.update({"_id"=>item["_id"]},item)
			rescue
			end
		end
	end
	def convert5
		@level5.find.each do |item|
			begin
				p "convert level5 #{item['_id']}"
				item["level4_id"] = item["levle4"]["_id"]
				@level5.update({"_id"=>item["_id"]},item)
			rescue
			end
			begin
				item.delete("levle4")
				item.delete("level4")
				item.delete("level5")
				@level5.update({"_id"=>item["_id"]},item)
			rescue
				p "convert level5 #{item['_id']}:error"
			end
		end
	end			
	def convert6
		@level6.find.each do |item|
			begin
				p "convert level6 #{item['_id']}"
				item["level5_id"] = item["level5"]["_id"]
				@level6.update({"_id"=>item["_id"]},item)
			rescue
				p "convert level6 #{item['_id']}:error"
			end
			begin
				item.delete("level5")
				@level6.update({"_id"=>item["_id"]},item)
			rescue
				p "convert level6 #{item['_id']}:error"
			end
		end
	end	
	def validate1
	end			
	def login
		page = @agent.get(@login)
		form =  page.forms.first
		form.UserName = @user[:name]
		form.passwd = @user[:pw]
		page = @agent.submit(form,form.buttons.first)
		p page
	end
	def test
		page = @agent.get("http://www.google.com")
		# page = page.link_with(:text=>'Gmail').click
		form = page.form('f')
		form.q = "gabriel"
		page = @agent.submit(form,form.buttons.first)
	end
	def guest
		p @agent.get(@guest)
	end
	def export_to_mysql_level4
		# @conn.query("insert into `tbl_exams` (_id,parent_id,title,href) values ('_id','parent_id','a','b')")
		@level4.find.each do |i|
			# @level4.update({"_id"=>i["_id"]},{"$set" =>{:export=>false}})
			# next
			begin
				result = @conn.query("select * from `tbl_exams` where `title`='#{i['name']}' and `href`='#{i['href']}';");
				if result.size ==0
					p 'success:'+i['name']
					@conn.query("insert into `tbl_exams` (_id,parent_id,title,href,publish) values ('#{i['_id']}','#{i['level3_id']}','#{i['name']}','#{i['href']}','#{i['publish']}')")
					@level4.update({"_id"=>i["_id"]},{"$set" =>{:export=>'exist'}})
				else
					p 'failed:'+i['name']
				end
			rescue
				p 'failed:'+i['name']
				@level4.update({"_id"=>i["_id"]},{"$set" =>{:export=>false}})
			end
		end
	end
	def export_to_mysql_level5
		@level5.find.each do |i|
			begin
				result = @conn.query("select * from `tbl_exams_items` where `ajax_id`='#{i['id']}' and `parent_id`='#{i['level4_id']}';");
				if result.size ==0
					p 'success:'+i['title']
					@conn.query("insert into `tbl_exams_items` (_id,parent_id,title,allnum,val,value,ajax_id) values ('#{i['_id']}','#{i['level4_id']}','#{i['title']}','#{i['allnum']}','#{i['val']}','#{i['value']}','#{i['id']}')")
					@level5.update({"_id"=>i["_id"]},{"$set" =>{:export=>'exist'}})
				else
					p 'failed:'+i['title']
				end
			rescue
				p 'failed:'+i['title']
				@level5.update({"_id"=>i["_id"]},{"$set" =>{:export=>false}})
			end
		end
	end
	def export_to_mysql_level6
		@level6.find.each do |i|
			begin
				result = @conn.query("select * from `tbl_questions_raw` where `parent_id`='#{i['level5_id']}';");
				if result.size ==0
					p 'success:'+i['level5_id'].to_s
					@conn.query("insert into `tbl_questions_raw` (_id,parent_id,raw) values ('#{i['_id']}','#{i['level5_id']}','#{i['row']}')")
					@level5.update({"_id"=>i["_id"]},{"$set" =>{:export=>'exist'}})
				else
					p 'failed:'+i['title'].to_s
				end
			rescue
				p 'failed:'+i['level5_id'].to_s
				@level5.update({"_id"=>i["_id"]},{"$set" =>{:export=>false}})
			end
		end
	end
	def export_to_new_ks
		children = @conn2.query("select * from `tbl_exams_items`")
		children.each do |child|
			parents = @conn2.query("select * from `tbl_exams` where `id` = '#{child['parent_id']}'")
			if parents.size ==0
				next
			end
			parent = parents.first
			query = "select * from `tbl_exams` where `href`='#{parent['href']}';"
			p query
			new_parents = @conn.query(query)
			if new_parents.size ==0
				next
			end
			obj = {
					'title' =>child['title'],
					'allnum'=>child['allnum'],
					'parent_id'=>new_parents.first['id'],
					'val'=>child['val'],
					'value'=>child['value'],
					'ajax_id'=>child['ajax_id']
				}
			save_level5(obj)
		end
	end
	def export_to_new_ks_2
		1.upto 63347 do |i|
			begin
				query = "select * from `tbl_questions_raw` limit #{(i-1)},1;"
				children = @conn2.query(query)
				children.each do |child|
					parents = @conn2.query("select * from `tbl_exams_items` where `_id` = '#{child['parent_id']}'")
					parents.each do |parent|

						query = "select * from `tbl_exams_items` where `ajax_id`='#{parent['ajax_id']}';"
						p query
						new_parents = @conn.query(query)
						p new_parents.first['id']

						if new_parents.size !=0
							obj = {
								'parent_id'=>new_parents.first['id'],
								'raw'=>child['raw']
							}
							# p obj["parent_id"]
							save_level6(obj)
						end
					end
				end
			rescue
			end
		end
	end
	def conver_src(src)
		dest = src.gsub("://","_").gsub('/','_')
	end
	def fetch_images
		result = @conn.query("select * from `tbl_questions_items_raw` where `src`!='' and `src_status`='' order by 'id' limit 10000,100")
		result.each do |row|
			begin
				images = JSON.parse(row['src'])
				images.each do |image|
					dest = conver_src(image)
					if !image.include? 'http://'
						image = "http://ks.233.com/#{image}"
					end
					p image
					File.open('./images/'+dest, 'wb') do |fo|
			  			fo.write open(image).read 
					end
				end
				@conn.query("update `tbl_questions_items_raw` set `src_status`='finished' where `id`=#{row['id']}")
			rescue
				p "error"
				# fetch_images
			end
		end
		fetch_images
	end
	def create_tree_relations
		result = @conn.query("select * from `tbl_tree` where `parent_id`='';")
		result.each do |row|
			create_tree_relations_row(row)
		end
	end
	def create_tree_relations_row(row)
		p row
		children = @conn.query("select * from `tbl_tree` where parent_id='#{row['id']}';")
		children.each do |child|
			@conn.query("insert into `tbl_tree_relations` (parent_id,child_id) values ('#{row['id']}','#{child['id']}')")
			create_tree_relations_row(child)
		end
	end
	def create_tree_menus
		root = @conn.query("select * from `tbl_tree` where `parent_id`='';")
		root.each do |row|
			query = "insert into `tbl_menus` (title,href) values ('#{row['title']}','#{@conn.escape(row['href'])}');"
			@conn.query(query)
			new_menu = @conn.query("select * from `tbl_menus` where title = '#{row['title']}' and href = '#{row['href']}';")
			id_1 =  new_menu.first['id']
			subs = @conn.query("select * from `tbl_tree` where parent_id='#{row['id']}';")
			subs.each do |sub|
				@conn.query("insert into `tbl_sub_menus` (title,parent_id,href) values ('#{sub['title']}','#{id_1}','#{sub['href']}');")
				new_sub = @conn.query("select * from `tbl_sub_menus` where href = '#{sub['href']}';")
				id_2 = new_sub.first['id']
				edges = @conn.query("select * from `tbl_tree` where parent_id='#{sub['id']}';")
				edges.each do |edge|
					@conn.query("insert into `tbl_edge_menus` (title,parent_id,href) values ('#{edge['title']}','#{id_2}','#{edge['href']}');")
				end
			end
		end
	end
	# def export_to_new_ks
	# 	exams = @conn2.query("select * from `tbl_exams` ")
	# 	exams.each do |item|
	# 		_id = item['id']
	# 		item_new = @conn.query("select * from `tbl_exams` where `title`='#{item['title']}' and `href`='#{item['href']}'")
	# 		parent_id = item_new.collect {|i|i['id']}.first
	# 		if parent_id == nil
	# 			# @conn2.query("update `tbl_exams` set `status`='false' where `id`='#{item['id']}';")
	# 			next
	# 		end
	# 		children = @conn2.query("select * from `tbl_exams_items` where `parent_id` = '#{item['_id']}'")
	# 		children.each do |child|
	# 			obj = {
	# 				'title' =>child['title'],
	# 				'allnum'=>child['allnum'],
	# 				'parent_id'=>parent_id,
	# 				'val'=>child['val'],
	# 				'value'=>child['value'],
	# 				'ajax_id'=>child['ajax_id']
	# 			}
	# 			save_level5(obj) 
	# 		end
	# 	end
	# end

end
ks = Ks233.new
# ks.fetch_images
# ks.create_tree_relations
# ks.curl_level5
# ks.curl_top_simulate
ks.create_tree_menus
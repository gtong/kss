require 'rubygems'
require 'mechanize'
require 'uri'
require 'mongo'
require 'mysql2'
class Ks
	def initialize
		@db = Mongo::Connection.new("localhost",27017).db("ks")
		@level1 = @db.collection("level1")
		@level2 = @db.collection("level2")
		@level3 = @db.collection("level3")
		@level4_history = @db.collection("level4_history")
		@level4 = @db.collection("level4")
		@level5 = @db.collection("level5")
		@level6 = @db.collection("level6")
		@error = @db.collection("error")
		@root = "http://ks.233.com/"
		@login = "http://wx.233.com/login/"
		@guest = "http://wx.233.com/search/UserCenter/?guest=1"
		@all = "http://ks.233.com/all/"
		@redirect = "http://wx.233.com/search/member/visitors.asp?redirectURL="
		@agent = Mechanize.new
		# add_cookie
		# p @agent.cookie_jar
		@user = {:name=>"gabrieltong",:pw=>"ty19830222"}
		# guest()
		setup_mysql()
	end
	def setup_mysql
		@conn = Mysql2::Client.new(:host => "127.0.01", :username => "root",:password=>'',:database=>'yii-ks')
	end
	def save_level1(obj)
		_save(@level1,obj)
	end
	def save_level2(obj)
		_save(@level2,obj)
	end
	def save_level3(obj)
		_save(@level3,obj)
	end
	def save_level4(obj)
		_save(@level4,obj)
	end
	def save_level5(obj)
		item = @level5.find(obj)
		if item.to_a.size == 0
			@level5.save(obj) 
			p "Save level5 successful #{obj['href']}"
		else
			p "Save level5 failed : existed #{obj['href']}"
		end
							
	end
	def save_level6(obj)
		item = @level6.find(obj)
		if item.to_a.size == 0
			@level6.save(obj) 
			# p obj[:level5]
			p "Save level6 successful "
		else
			p "Save level6 failed : existed "
		end				
	end
	def _save(collection,obj)
		if obj[:href]=="#" || obj[:name]=="" 
			return
		end
		item = collection.find({:name=>obj[:href]})
		collection.save(obj) if item.to_a.size == 0		
	end
	def add_cookie
		cookies = [
			{
				:key=>"CNZZDATA1527823",
				:value=>"cnzz_eid=32793556-1347412656-http%253A%252F%252Fks.233.com%252F&ntime=1347412656&cnzz_a=1&retime=1347416444079&sin=http%253A%252F%252Fks.233.com%252F&ltime=1347416444079&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"CNZZDATA3583776",
				:value=>"cnzz_eid=91239576-1347412787-http%253A%252F%252Fwx.233.com%252Fsearch%252FUserCenter%252F&ntime=1347412787&cnzz_a=1&retime=1347416458061&sin=none&ltime=1347416458061&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"CNZZDATA4109769",
				:value=>"cnzz_eid=32069504-1347416404-http%253A%252F%252Fwx.233.com%252Fsearch%252FUserCenter%252F&ntime=1347416404&cnzz_a=1&retime=1347416458034&sin=none&ltime=1347416458034&rtime=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"Dozedu%5Fcom",
				:value=>"L%5FBgNum=0&Guide=1&IsVIP=0&Username=javascriptdaily&PassWord=bdfed55722947570347ed5261d2ece6a&LastLoginTime=2012%2F9%2F12+10%3A19%3A59&LastLoginIP=210%2E177%2E98%2E223&OrderNum=0&MemberLevel=0&CourseNum=0&Eb=0&Money=0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"233AspNet",
				:value=>"UN=javascriptdaily&mcount=8&nowmcount=8&t=2012/9/12 10:20:05",
				:domain=>"wx.233.com"
			},
			{
				:key=>"233doNet",
				:value=>"TaskNum=17",
				:domain=>"wx.233.com"
			},
			{
				:key=>"ASP.NET_SessionId",
				:value=>"xh54pzq0qb5jqtybmavysa55",
				:domain=>"wx.233.com"
			},
			{
				:key=>"ASPSESSIONIDCSBDSSRD",
				:value=>"LDDDIENDEANGGIIJHKGECAFF",
				:domain=>"wx.233.com"
			},
			{
				:key=>"LogErrNum",
				:value=>"0",
				:domain=>"wx.233.com"
			},
			{
				:key=>"jsusername",
				:value=>"javascriptdaily",
				:domain=>"wx.233.com"
			},
			{
				:key=>"233%5Fcom",
				:value=>"CourseNum=0&OrderNum=0&password=bdfed55722947570347ed5261d2ece6a&username=javascriptdaily",
				:domain=>".233.com"
			},
			{
				:key=>"233Data",
				:value=>"password=bdfed55722947570347ed5261d2ece6a&username=javascriptdaily",
				:domain=>".233.com"
			},
			{
				:key=>"wxUserName",
				:value=>"javascriptdaily",
				:domain=>".233.com"
			}
		]
		# cookies = [
		# 	{
		# 		:key=>"ASPSESSIONIDQCSTADSS",
		# 		:value=>"CNMOPCICPGOEEGLMIKIGJGAL",
		# 		:domain=>"ks.233.com"
		# 	},
		# 	{
		# 		:key=>"CNZZDATA840328",
		# 		:value=>"cnzz_eid=68180478-1347288987-&ntime=1347288987&cnzz_a=1&retime=1347289445563&sin=&ltime=1347289445563&rtime=0",
		# 		:domain=>"ks.233.com"
		# 	},
		# 	{
		# 		:key=>"Hm_lpvt_8c45d7fed7660d80a79442b673daef31",
		# 		:value=>"1347289446278",
		# 		:domain=>"ks.233.com"
		# 	},
		# 	{
		# 		:key=>"Hm_lvt_8c45d7fed7660d80a79442b673daef31",
		# 		:value=>"1347289430152,1347289446278",
		# 		:domain=>"ks.233.com"
		# 	},
		# 	{
		# 		:key=>"233Data",
		# 		:value=>"password=54c0a14fc7e1a4a219ea0e9ca4bc5288&username=gabrieltong",
		# 		:domain=>".233.com"
		# 	},
		# 	{
		# 		:key=>"wxUserName",
		# 		:value=>"gabrieltong",
		# 		:domain=>".233.com"
		# 	},
		# 	{
		# 		:key=>"CNZZDATA1527823",
		# 		:value=>"cnzz_eid=82798369-1347280392-&ntime=1347280392&cnzz_a=0&retime=1347289111228&sin=&ltime=1347289111228&rtime=0",
		# 		:domain=>"wx.233.com"
		# 	},
		# 	{
		# 		:key=>"CNZZDATA3583776",
		# 		:value=>"http%253A%252F%252Fwx.233.com%252Flogin%252F&ntime=1347289130&cnzz_a=1&retime=1347289435593&sin=none&ltime=1347289435593&rtime=0",
		# 		:domain=>"wx.233.com"
		# 	},
		# 	{
		# 		:key=>"CNZZDATA4109769",
		# 		:value=>"cnzz_eid=53723152-1347289130-http%253A%252F%252Fwx.233.com%252Flogin%252F&ntime=1347289130&cnzz_a=1&retime=1347289435624&sin=none&ltime=1347289435624&rtime=0",
		# 		:domain=>"wx.233.com"
		# 	},
		# 	{
		# 		:key=>"Dozedu%5Fcom",
		# 		:value=>"L%5FBgNum=0&Money=0&Eb=0&CourseNum=0&MemberLevel=0&OrderNum=0&LastLoginIP=210%2E177%2E98%2E223&LastLoginTime=2012%2F9%2F10+17%3A31%3A02&PassWord=54c0a14fc7e1a4a219ea0e9ca4bc5288&Guide=1&IsVIP=0&Username=gabrieltong",
		# 		:domain=>"wx.233.com"
		# 	}
		# ]
		cookies.each do |c|
			cookie = Mechanize::Cookie.new(c[:key], c[:value])
			cookie.path = "/"
			cookie.domain = c[:domain]
			cookie.for_domain = true
			@agent.cookie_jar.add!(cookie)
		end
	end
	def curl_level1
		page = @agent.get(@all)
		types = page.search(".shiti_list .title0 a")
		types.each do |a|
			obj = {"name"=>a.content,"href"=>a.attr("href")}
			save_level1(obj)
		end

	end
	def curl_level2
		@level1.find.each do |l|
			page = @agent.get(@root+l["href"])
			page.search(".most-s-c a").each_with_index do |link,i|
				if i==0
					next
				end
				obj = {"name"=>link.content,"href"=>link.attr("href"),"level1"=>l}
				@level2.save(obj)
			end
		end
	end
	def curl_level3
		@level2.find.each do |l|
			p l["href"]
			next
			page = @agent.get(l["href"])
			page.search(".most-s-c a").each_with_index do |link,i|
				if i==0
					next
				end
				obj = {"name"=>link.content,"href"=>link.attr("href"),"level2"=>l}
				@level3.save(obj)
			end
		end
	end
	def curl_level4
		@level3.find({:finished=>false}).each do |l|
			p "Curling Level4 #{l['name']}:#{l['href']}"
			curl_level4_item(l['href'],l)
			@level3.update({"_id"=>l["_id"]},{"$set" =>{:finished=>true}})
		end
	end
	def curl_level4_item(url,level)
		
		if @level4_history.find({"url"=>url,"finished"=>true}).to_a.size!=0
			return
		end
		begin
			page = @agent.get(url)
		rescue
			p "Curl #{url} failed"
			@error.save({:href=>url,:type=>"curl_level4_item"})
			return
		end
		p "Curl #{url} started"
		page.search(".shiti_list .hangs").each do |item|
			begin 
				href = item.css("a")[1].attr("href")
				name = item.css('a')[1].content
				count = item.css(".tite_2")[0].content
				publish = item.css(".w75")[-1].content
				obj = {
					:href =>href,
					:name =>name,
					:count=>count,
					:publish=>publish,
					:level3=>level
				}
				save_level4(obj)
				p "Curl #{href} successful"
			rescue
				p "Curl #{href} failed"
				@error.save({:href=>url,:type=>"curl_level4_item"})
			end
		end
		@level4_history.save({:url=>url,:finished=>true})
		manu = page.search(".manu")
		begin
			current = manu.search(".current")[0].content.to_i
			manu.search("a").each do |a|
				num = a.content.to_i
				if num>current
					curl_level4_item(a.attr('href'),level)
				end
			end
		rescue
			@error.save({:href=>url,:type=>"curl_level4_pagination"})
		end
	end
	def curl_level5
		@level4.find({"finished"=>false}).each do |item|			
			if item["finished"] !=false
				p "Already finished"
				next
			end
			begin			
				p "."*100
				p "Curl Level5 #{item['href']} start "
				@level4.update({"_id"=>item["_id"]},{"$set" =>{:finished=>"locked"}})
				page = @agent.get(@redirect+item["href"])
				i = 0
				page.search("#xuanz input").each do |input|
					obj = {
						"title"=>input.attr("title"),
						"value"=>input.attr("value"),
						"id"=>input.attr("id"),
						"val"=>input.attr("val"),
						"allnum"=>input.attr("allnum"),
						"levle4"=>item
					}
					i = i+1
					save_level5(obj)
				end
				if i!=0
					p "Curl Level5 #{item['href']} successful "
					@level4.update({"_id"=>item["_id"]},{"$set" =>{:finished=>true}})
				else
					p "Curl Level5 #{item['href']} failed : no tab found "
				end
			rescue
				p "Curl Level5 #{item['href']} failed : can't open page"
				@error.save({:href=>item["href"],:type=>"curl_level5_tabs"})
			end
		end
	end
	def curl_level6
		begin
			curl_level6_item
		rescue
			curl_level6
		end
	end
	def curl_level6_item
		@level5.find({"finished"=>false}).each do |item|
			begin
				@level5.update({"_id"=>item["_id"]},{"$set" =>{:finished=>"locked"}})
				# p item["levle4"]["href"]
				pageID = item["levle4"]["href"].gsub("http://wx.233.com/search/UserCenter/examCenter/answer.asp?PaperID=","").gsub("&guest=1","").to_i
				# p item
				href = "http://wx.233.com/search/UserCenter/task/exam/ExamServer.ashx?Act=ViewAnswer&RulesID=#{item['id']}&PaperID=#{pageID}&_=1347410627224"
				# p href
				page = @agent.get(href)

				row = page.search("body")[0].content
				obj ={:row=>row,:level5=>item}
				p obj
				if obj.to_s == "{\"S\":\"0\"}"
					p "Failed"
					@error.save({:href=>item["href"],:type=>"curl_level6"})
					next
				end
				save_level6(obj)
				@level5.update({"_id"=>item["_id"]},{"$set" =>{:finished=>true}})
			rescue
				@error.save({:href=>item["href"],:type=>"curl_level6"})
			end
		end
	end
	def convert2
		@level2.find.each do |item|
			begin
				p "convert level2 #{item['_id']}"
				item["level1_id"] = item["level1"]["_id"]
				item.delete("level1")
				@level2.update({"_id"=>item["_id"]},item)
			rescue
			end
		end
	end
	def convert3
		@level3.find.each do |item|
			begin
				p "convert level3 #{item['_id']}"
				item["level2_id"] = item["level2"]["_id"]
				@level3.update({"_id"=>item["_id"]},item)
			rescue
			end
			begin
				item.delete("level2")
				@level3.update({"_id"=>item["_id"]},item)
			rescue
			end
			
		end
	end	
	def convert4
		@level4.find.each do |item|
			begin
				p "convert level4 #{item['_id']}"
				item["level3_id"] = item["level3"]["_id"]
				@level4.update({"_id"=>item["_id"]},item)
			rescue
			end
			begin
				item.delete("level3")
				item.delete("finished")
				@level4.update({"_id"=>item["_id"]},item)
			rescue
			end
		end
	end
	def convert5
		@level5.find.each do |item|
			begin
				p "convert level5 #{item['_id']}"
				item["level4_id"] = item["levle4"]["_id"]
				@level5.update({"_id"=>item["_id"]},item)
			rescue
			end
			begin
				item.delete("levle4")
				item.delete("level4")
				item.delete("level5")
				@level5.update({"_id"=>item["_id"]},item)
			rescue
				p "convert level5 #{item['_id']}:error"
			end
		end
	end			
	def convert6
		@level6.find.each do |item|
			begin
				p "convert level6 #{item['_id']}"
				item["level5_id"] = item["level5"]["_id"]
				@level6.update({"_id"=>item["_id"]},item)
			rescue
				p "convert level6 #{item['_id']}:error"
			end
			begin
				item.delete("level5")
				@level6.update({"_id"=>item["_id"]},item)
			rescue
				p "convert level6 #{item['_id']}:error"
			end
		end
	end	
	def validate1
	end			
	def login
		page = @agent.get(@login)
		form =  page.forms.first
		form.UserName = @user[:name]
		form.passwd = @user[:pw]
		page = @agent.submit(form,form.buttons.first)
		p page
	end
	def test
		page = @agent.get("http://www.google.com")
		# page = page.link_with(:text=>'Gmail').click
		form = page.form('f')
		form.q = "gabriel"
		page = @agent.submit(form,form.buttons.first)
	end
	def guest
		p @agent.get(@guest)
	end
	def export_to_mysql_level4
		# @conn.query("insert into `tbl_exams` (_id,parent_id,title,href) values ('_id','parent_id','a','b')")
		@level4.find.each do |i|
			# @level4.update({"_id"=>i["_id"]},{"$set" =>{:export=>false}})
			# next
			begin
				result = @conn.query("select * from `tbl_exams` where `title`='#{i['name']}' and `href`='#{i['href']}';");
				if result.size ==0
					p 'success:'+i['name']
					@conn.query("insert into `tbl_exams` (_id,parent_id,title,href,publish) values ('#{i['_id']}','#{i['level3_id']}','#{i['name']}','#{i['href']}','#{i['publish']}')")
					@level4.update({"_id"=>i["_id"]},{"$set" =>{:export=>'exist'}})
				else
					p 'failed:'+i['name']
				end
			rescue
				p 'failed:'+i['name']
				@level4.update({"_id"=>i["_id"]},{"$set" =>{:export=>false}})
			end
		end
	end
	def export_to_mysql_level5
		@level5.find.each do |i|
			begin
				result = @conn.query("select * from `tbl_exams_items` where `ajax_id`='#{i['id']}' and `parent_id`='#{i['level4_id']}';");
				if result.size ==0
					p 'success:'+i['title']
					@conn.query("insert into `tbl_exams_items` (_id,parent_id,title,allnum,val,value,ajax_id) values ('#{i['_id']}','#{i['level4_id']}','#{i['title']}','#{i['allnum']}','#{i['val']}','#{i['value']}','#{i['id']}')")
					@level5.update({"_id"=>i["_id"]},{"$set" =>{:export=>'exist'}})
				else
					p 'failed:'+i['title']
				end
			rescue
				p 'failed:'+i['title']
				@level5.update({"_id"=>i["_id"]},{"$set" =>{:export=>false}})
			end
		end
	end
	def export_to_mysql_level6
		@level6.find.each do |i|
			begin
				result = @conn.query("select * from `tbl_questions_raw` where `parent_id`='#{i['level5_id']}';");
				if result.size ==0
					p 'success:'+i['level5_id'].to_s
					@conn.query("insert into `tbl_questions_raw` (_id,parent_id,raw) values ('#{i['_id']}','#{i['level5_id']}','#{i['row']}')")
					@level5.update({"_id"=>i["_id"]},{"$set" =>{:export=>'exist'}})
				else
					p 'failed:'+i['title'].to_s
				end
			rescue
				p 'failed:'+i['level5_id'].to_s
				@level5.update({"_id"=>i["_id"]},{"$set" =>{:export=>false}})
			end
		end
	end
end
ks = Ks.new
# ks.convert1
# ks.convert2
# ks.convert3
# ks.convert6
ks.export_to_mysql_level6